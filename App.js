import React from 'react';
import { View } from 'react-native'
import CreateAlarm from './components/CreateAlarm'
import Home from './components/Home'
import ShakeViewer from './components/ShakeViewer'
import About from './components/About'
import MathViewer from './components/MathViewer'
import PicViewer from './components/PicViewer'
import { Router, Scene } from 'react-native-router-flux'
import { connect } from 'react-redux'

class App extends React.Component {

  render() {
    return (
      <Router>
        <Scene key="root" headerMode="none">
          <Scene key="home" component={Home} initial />
          <Scene key="createAlarm" component={CreateAlarm} />
          <Scene key="mathViewer" component={MathViewer} />
          <Scene key="shakeViewer" component={ShakeViewer} />
          <Scene key="picViewer" component={PicViewer} />
          <Scene key="about" component={About} />
        </Scene>
      </Router >
    );
  }
}

const mapStateToDispatch = state => ({
  currentAlarm: state.home.currentAlarm,
})

export default connect(mapStateToDispatch)(App)