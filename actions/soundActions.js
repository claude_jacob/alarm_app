import * as types from '../actionTypes/soundActionTypes'

export const setCurrentAlarmSound = sound => {
    return dispatch => {
        dispatch({ type: types.SET_CURRENT_ALARM_SOUND, sound: sound })
    }
}

export const removeSound = () => {
    return dispatch => {
        dispatch({ type: types.REMOVE_SOUND })
    }
}