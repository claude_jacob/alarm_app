import * as types from '../actionTypes/appActionTypes'

export const isLoading = value => {
    return dispatch => {
        dispatch({ type: types.IS_LOADING, value: value })
    }
}