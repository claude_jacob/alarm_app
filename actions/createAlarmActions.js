import * as types from '../actionTypes/createAlarmActionTypes'

export const updateDate = date => {
    return dispatch => {
        dispatch({ type: types.UPDATE_DATE, date: date })
    }
}

export const showDateTimePicker = value => {
    return dispatch => {
        dispatch({ type: types.SHOW_DATETIME_PICKER, value: value })
    }
}

export const upadteRingtone = ringtone => {
    return dispatch => {
        dispatch({ type: types.UPDATE_RINGTONE, ringtone: ringtone })
    }
}

export const playRingtone = status => {
    return dispatch => {
        dispatch({ type: types.CHANGE_RINGTONE_STATUS, status: status })
    }
}

export const changeModalVisible = () => {
    return dispatch => {
        dispatch({ type: types.CHANGE_MODAL_VISIBLE })
    }
}

export const editAlarm = alarm => {
    return dispatch => {
        dispatch({ type: types.EDIT_ALARM, repeat: alarm.repeat, hour: alarm.hour, minute: alarm.minute, daytime: alarm.daytime, ringtone: alarm.ringtone })
    }
}

export const handleAlarmNameChange = text => {
    return dispatch => {
        dispatch({ type: types.ALARM_NAME_CHANGE, alarmName: text })
    }
}

export const changeAlarmType = type => {
    return dispatch => {
        if (type === 'shake') {
            dispatch({ type: types.CHANGE_ALARM_TYPE, checkShake: true, checkMath: false, checkPics: false, alarmType: 'shake' })
        }
        if (type === 'math') {
            dispatch({ type: types.CHANGE_ALARM_TYPE, checkShake: false, checkMath: true, checkPics: false, alarmType: 'math' })
        }
        if (type === 'pics') {
            dispatch({ type: types.CHANGE_ALARM_TYPE, checkShake: false, checkMath: false, checkPics: true, alarmType: 'pics' })
        }
    }
}

export const updateShakeCount = count => {
    return dispatch => {
        dispatch({ type: types.UPDATE_SHAKE_COUNT, count: count })
    }
}

export const updateDifficulty = difficulty => {
    return dispatch => {
        dispatch({ type: types.UPDATE_DIFFICULTY, difficulty: difficulty })
    }
}