import * as types from '../actionTypes/homeActionTypes'

export const createAlarm = alarm => {
    return (dispatch) => {
        dispatch({ type: types.ADD_ALARM, alarm: alarm })
    }
}

export const changeActive = (index) => {
    return dispatch => {
        dispatch({ type: types.CHANGE_ACTIVE, index: index })
    }
}

export const reloadComponent = () => {
    return (dispatch) => {
        dispatch({ type: types.RELOAD })
    }
}

export const deleteAlarm = (index) => {
    return dispatch => {
        dispatch({ type: types.DELETE_ALARM, index: index })
    }
}


export const deleteAlarmById = id => {
    return dispatch => {
        dispatch({ type: types.DELETE_ALARM_BY_ID, id: id })
    }
}

export const saveEditedAlarm = (alarm, index) => {
    return dispatch => {
        dispatch({ type: types.SAVE_EDITED_ALARM, alarm: alarm, index: index })
    }
}

export const addTime = () => {
    return dispatch => {
        dispatch({ type: types.UPDATE_TIME })
    }
}

export const detectActiveAlarms = () => {
    return dispatch => {
        dispatch({ type: types.DETECT_ACTIVE_ALARMS })
    }
}

export const updateShakeCount = count => {
    return dispatch => {
        dispatch({ type: types.UPDATE_SHAKE_COUNT, count: count })
    }
}

export const changeIsAlarming = value => {
    return dispatch => {
        dispatch({ type: types.ALARMING, value: value })
    }
}

export const setCurrentAlarm = alarm => {
    return dispatch => {
        dispatch({ type: types.SET_CURRENT_ALARM, alarm: alarm })
    }
}

export const deleteCurrentAlarm = () => {
    return dispatch => {
        dispatch({ type: types.DELETE_CURRENT_ALARM })
    }
}

export const resetAlarms = () => {
    return dispatch => {
        dispatch({ type: types.RESET_ALARMS })
    }
}

export const setVolumeListener = listener => {
    return dispatch => {
        dispatch({ type: types.SET_VOLUME_LISTENER, listener: listener })
    }
}

export const removeVolumeListener = () => {
    return dispatch => {
        dispatch({ type: types.REMOVE_VOLUME_LISTENER })
    }
}

export const addToCurrentAlarms = alarm => {
    return dispatch => {
        dispatch({ type: types.ADD_TO_CURRENT_ALARMS, alarm: alarm })
    }
}

export const resetCurrentAlarmsList = () => {
    return dispatch => {
        dispatch({ type: types.RESET_CURRENT_ALARMS })
    }
}

export const changeIsHome = value => {
    return dispatch => {
        dispatch({ type: types.CHANGE_IS_HOME, value: value })
    }
}

export const didFinishAlarmDismiss = value => {
    return dispatch => {
        dispatch({ type: types.CHANGE_DID_FINISH_ALARM_DISMISS, value: value })
    }
}