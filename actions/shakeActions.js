import * as types from '../actionTypes/shakeActionTypes'

export const updateShake = () => {
    return dispatch => {
        dispatch({ type: types.UPDATE_SHAKE })
    }
}

export const resetShake = () => {
    return dispatch => {
        dispatch({ type: types.RESET_SHAKE })
    }
}

export const setShake = count => {
    return dispatch => {
        dispatch({ type: types.SET_SHAKE, count: count })
    }
}

export const setShakeAlarm = alarm => {
    return dispatch => {
        dispatch({ type: types.SET_ALARM, alarm: alarm })
    }
}

export const removeAlarm = () => {
    return dispatch => {
        dispatch({ type: types.REMOVE_ALARM })
    }
}