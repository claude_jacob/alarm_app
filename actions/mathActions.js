import * as types from '../actionTypes/mathActionTypes'

export const handleTextChange = text => {
    return dispatch => {
        dispatch({ type: types.HANDLE_ANSWER_TEXT_CHANGE, text: text })
    }
}

export const changeSubmitted = value => {
    return dispatch => {
        dispatch({ type: types.CHANGE_SUBMITTED, value: value })
    }
}

export const setMath = math => {
    return dispatch => {
        dispatch({ type: types.SET_MATH, math: math })
    }
}

export const resetMath = () => {
    return dispatch => {
        dispatch({ type: types.RESET_MATH })
    }
}

export const setMathAlarm = alarm => {
    return dispatch => {
        dispatch({ type: types.SET_ALARM, alarm: alarm })
    }
}

export const removeAlarm = alarm => {
    return dispatch => {
        dispatch({ type: types.REMOVE_ALARM })
    }
}