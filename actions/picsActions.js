import * as types from '../actionTypes/picsActionTypes'

export const updateUserAnswer = text => {
    return dispatch => {
        dispatch({ type: types.UPDATE_USER_ANSWER, text: text })
    }
}

export const setGame = game => {
    return dispatch => {
        dispatch({ type: types.SET_GAME, game: game })
    }
}

export const setPicAlarm = alarm => {
    return dispatch => {
        dispatch({ type: types.SET_ALARM, alarm: alarm })
    }
}

export const reset = () => {
    return dispatch => {
        dispatch({ type: types.RESET })
    }
}

export const changeSubmitted = value => {
    return dispatch => {
        dispatch({ type: types.CHANGE_SUBMIT, value: value })
    }
}