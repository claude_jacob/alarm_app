Include the code below in node_modules/react-native-alarm-notification/android/src/main/java/com/emakelites/react/native/alarm/notification/ANAlarmReceiver.java
to launch the App at designated time trigger.

<code>
 @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent();
        i.setClassName("com.alarm_app", "com.alarm_app.MainActivity");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
        
        ...
        }
</code> 