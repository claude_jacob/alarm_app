import * as types from '../actionTypes/homeActionTypes'

const initialState = {
    alarms: [],
    reload: false,
    time: 0,
    activeAlarms: [],
    shake: 0,
    isAlarming: false,
    currentAlarm: null,
    atHome: false,
    finishedAlarmDismiss: true,
    sound: null
}

const createAlarm = (state = initialState, action) => {
    switch (action.type) {
        case types.RELOAD:
            return { ...state, reload: !state.reload }
        case types.ADD_ALARM:
            return { ...state, alarms: state.alarms.concat(action.alarm) }
        case types.CHANGE_ACTIVE:
            return { ...state, alarms: state.alarms.map((item, index) => index === action.index ? { ...item, active: !item.active } : item) }
        case types.DELETE_ALARM:
            return { ...state, alarms: [...state.alarms.slice(0, action.index), ...state.alarms.slice(action.index + 1)] }
        case types.SAVE_EDITED_ALARM:
            return { ...state, alarms: state.alarms.map((item, index) => index === action.index ? action.alarm : item) }
        case types.UPDATE_TIME:
            return { ...state, time: state.time += 1 }
        case types.DETECT_ACTIVE_ALARMS:
            return { ...state, activeAlarms: state.alarms.filter(alarm => alarm.active) }
        case types.UPDATE_SHAKE_COUNT:
            return { ...state, shake: action.count }
        case types.ALARMING:
            return { ...state, isAlarming: action.value }
        case types.SET_CURRENT_ALARM:
            return { ...state, currentAlarm: action.alarm }
        case types.DELETE_CURRENT_ALARM:
            return { ...state, currentAlarm: null }
        case types.DELETE_ALARM_BY_ID:
            return { ...state, alarms: state.alarms.filter(alarm => alarm.id !== action.id) }
        case types.RESET_ALARMS:
            return { ...state, alarms: [] }
        case types.CHANGE_IS_HOME:
            return { ...state, atHome: action.value }
        case types.CHANGE_DID_FINISH_ALARM_DISMISS:
            return { ...state, finishedAlarmDismiss: action.value }
        case types.SET_CURRENT_ALARM_SOUND:
            return { ...state, sound: action.sound }
        case types.REMOVE_SOUND:
            return { ...state, sound: null }
        default:
            return state
    }
}

export default createAlarm 