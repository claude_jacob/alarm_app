import * as types from '../actionTypes/appActionTypes'

const initialState = {
    loading: false
}

const app = (state = initialState, action) => {
    switch (action.type) {
        case types.IS_LOADING:
            return { ...state, loading: action.value }
        default:
            return state
    }
}

export default app