import * as types from '../actionTypes/picsActionTypes'

const initialState = {
    alarm: null,
    game: null,
    userAnswer: '',
    submitted: false
}

const pics = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_ALARM:
            return { ...state, alarm: action.alarm }
        case types.SET_GAME:
            return { ...state, game: action.game }
        case types.UPDATE_USER_ANSWER:
            return { ...state, userAnswer: action.text }
        case types.RESET:
            return { ...state, alarm: null, game: null, userAnswer: '', submitted: false }
        case types.CHANGE_SUBMIT:
            return { ...state, submitted: action.value }
        default:
            return state
    }
}

export default pics 