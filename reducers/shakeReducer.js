import * as types from '../actionTypes/shakeActionTypes'

const initialState = {
    shakeCount: 1,
    alarm: null
}

const createAlarm = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_SHAKE:
            return { ...state, shakeCount: action.count }
        case types.UPDATE_SHAKE:
            return { ...state, reload: state.shakeCount -= 1 }
        case types.RESET_SHAKE:
            return { ...state, shakeCount: 1 }
        case types.SET_ALARM:
            return { ...state, alarm: action.alarm }
        case types.REMOVE_ALARM:
            return { ...state, alarm: null }
        default:
            return state
    }
}

export default createAlarm 