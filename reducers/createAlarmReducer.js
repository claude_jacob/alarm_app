import * as types from '../actionTypes/createAlarmActionTypes'
import { default as Sound } from 'react-native-sound';
import { ALARM_AUDIO as alarmAudio } from '../assets/alarmDismissOptions'

const initialState = {
    date: new Date(),
    active: true,
    ringtone: alarmAudio[0],
    ringtonePlaying: false,
    modalVisible: false,
    alarmName: 'Alarm',
    isDateTimePickerVisible: false,
    checkShake: true,
    checkMath: false,
    checkPics: false,
    alarmType: 'shake',
    shakeCount: '30',
    difficulty: 'easy'
}

const createAlarm = (state = initialState, action) => {
    switch (action.type) {
        case types.UPDATE_DATE:
            return { ...state, date: action.date }
        case types.UPDATE_RINGTONE:
            return { ...state, ringtone: action.ringtone }
        case types.CHANGE_RINGTONE_STATUS:
            return { ...state, ringtonePlaying: action.status }
        case types.CHANGE_MODAL_VISIBLE:
            return { ...state, modalVisible: !state.modalVisible }
        case types.EDIT_ALARM:
            return { ...state, repeat: action.repeat, hour: action.hour, minute: action.minute, daytime: action.daytime, ringtone: action.ringtone }
        case types.ALARM_NAME_CHANGE:
            return { ...state, alarmName: action.alarmName }
        case types.SHOW_DATETIME_PICKER:
            return { ...state, isDateTimePickerVisible: action.value }
        case types.CHANGE_ALARM_TYPE:
            return { ...state, checkShake: action.checkShake, checkMath: action.checkMath, checkPics: action.checkPics, alarmType: action.alarmType }
        case types.UPDATE_SHAKE_COUNT:
            return { ...state, shakeCount: action.count }
        case types.UPDATE_DIFFICULTY:
            return { ...state, difficulty: action.difficulty }
        default:
            return state
    }
}
export default createAlarm 