import { combineReducers } from 'redux'
import createAlarm from './createAlarmReducer'
import home from './homeReducer'
import shake from './shakeReducer'
import app from './appReducer'
import math from './mathReducer'
import pics from './picsReducer'
import sound from './soundReducer'

export default combineReducers({
    createAlarm: createAlarm,
    home: home,
    shake: shake,
    app: app,
    math: math,
    pics: pics,
    sound: sound
})