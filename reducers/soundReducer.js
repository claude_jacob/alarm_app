import * as types from '../actionTypes/soundActionTypes'

const initialState = {
    sound: null
}

const sound = (state = initialState, action) => {
    switch (action.type) {
        case types.REMOVE_SOUND:
            return { ...state, sound: null }
        case types.SET_CURRENT_ALARM_SOUND:
            return { ...state, sound: action.sound }
        default:
            return state
    }
}

export default sound