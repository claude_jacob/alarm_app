import * as types from '../actionTypes/mathActionTypes'

const initialState = {
    answer: '',
    submitted: false,
    math: null,
    alarm: null,
}

const math = (state = initialState, action) => {
    switch (action.type) {
        case types.HANDLE_ANSWER_TEXT_CHANGE:
            return { ...state, answer: action.text }
        case types.CHANGE_SUBMITTED:
            return { ...state, submitted: action.value }
        case types.SET_MATH:
            return { ...state, math: action.math }
        case types.RESET_MATH:
            return { ...state, math: null, answer: '', submitted: false, alarm: null }
        case types.SET_ALARM:
            return { ...state, alarm: action.alarm }
        case types.REMOVE_ALARM:
            return { ...state, alarm: null }
        default:
            return state
    }
}

export default math