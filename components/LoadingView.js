import React from 'react'
import { View, StyleSheet, ActivityIndicator } from 'react-native'

export const LoadingView = () => (
    <View style={styles.loadingContainer}>
        <ActivityIndicator style={styles.loadingIndicator} />
    </View>
)


const styles = StyleSheet.create({
    loadingContainer: {
        display: 'flex',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
        backgroundColor: '#000000',
        opacity: 0.5,
    },
    loadingIndicator: {
        flex: 1,
        alignItems: 'center',
    }
})