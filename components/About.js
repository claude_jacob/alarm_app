import React from 'react'
import { View, Image, ImageBackground, ScrollView, TouchableOpacity, Linking } from 'react-native'
import { Card, Text, ListItem } from 'react-native-elements'

const About = () => (
    <ImageBackground source={require('../assets/background.png')} style={{ height: '100%', width: '100%' }}>
        <ScrollView>
            <View style={{ alignItems: 'center', alignContent: 'center' }}>
                <Card containerStyle={{ backgroundColor: 'rgba(255,255,255,0.2)', borderWidth: 0, borderRadius: 15 }} titleStyle={{ fontSize: 23, color: 'white' }} title={`Special thanks to`}>

                    <Card containerStyle={{ borderRadius: 5 }} image={require('../assets/profile/ervin.png')} >
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>
                            John Ervin V. Mascariñas
                    </Text>
                        <Text style={{ marginBottom: 10 }}>
                            Researcher
                    </Text>
                    </Card>
                    <Card containerStyle={{ borderRadius: 5 }} image={require('../assets/profile/veronica.png')} >
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>
                            Veronica S. Garcia
                    </Text>
                        <Text style={{ marginBottom: 10 }}>
                            Researcher
                    </Text>
                    </Card>
                    <Card containerStyle={{ borderRadius: 5 }} image={require('../assets/profile/winnie.png')} >
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>
                            Winnie Rose I. Correa
                    </Text>
                        <Text style={{ marginBottom: 10 }}>
                            Researcher
                    </Text>
                    </Card>
                    <Card containerStyle={{ borderRadius: 5 }} image={require('../assets/profile/john.png')} >
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>
                            John Louie Coronel
                    </Text>
                        <Text style={{ marginBottom: 10 }}>
                            Researcher
                    </Text>
                    </Card>
                    <Card containerStyle={{ borderRadius: 5 }} image={require('../assets/profile/salve.png')} >
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>
                            Ms. Salve Golez De Guzman
                    </Text>
                        <Text style={{ marginBottom: 10 }}>
                            Research Adviser
                    </Text>
                    </Card>
                    <TouchableOpacity activeOpacity={1} onLongPress={showProgrammersFacebook}>
                        <Card containerStyle={{ borderRadius: 5 }} image={require('../assets/profile/jacob.png')} >
                            <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>
                                Claude Jacob Aparecio
                    </Text>
                            <Text style={{ marginBottom: 10 }}>
                                Programmer
                    </Text>
                        </Card>
                    </TouchableOpacity>
                    <Text style={{ color: 'white', marginTop: 10, textAlign: 'center' }}>For any comments and suggestions please email or contact us at: </Text>
                    <Text style={{ color: 'white', textAlign: 'center' }}>ervinprogrammer17@gmail.com 09350568956 / 09174533804</Text>
                    <Text style={{ color: 'white', marginTop: 10, textAlign: 'center' }}>All rights reserved © TheUnstoppable 2019</Text>
                </Card>
            </View>
        </ScrollView>
    </ImageBackground>
)

const showProgrammersFacebook = () => {
    Linking.openURL('https://www.facebook.com/jikubb')
}
export default About