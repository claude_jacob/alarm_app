import React from 'react'
import { View, ScrollView, RCTNativeAppEventEmitter, ImageBackground, Alert, TouchableOpacity } from 'react-native'
import { Text, Card, ListItem, Icon, List } from 'react-native-elements'
import { Actions, ActionConst } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { didFinishAlarmDismiss, reloadComponent, changeActive, deleteAlarm, addTime, detectActiveAlarms, updateShakeCount, changeIsAlarming, setCurrentAlarm, deleteCurrentAlarm, resetAlarms, changeIsHome } from '../actions/homeActions'
import { setCurrentAlarmSound, removeSound } from '../actions/soundActions'
import { editAlarm } from '../actions/createAlarmActions'
import { setShakeAlarm, setShake } from '../actions/shakeActions'
import { setMathAlarm, setMath } from '../actions/mathActions'
import { setPicAlarm, setGame } from '../actions/picsActions'
import { isLoading } from '../actions/appActions'
import ReactNativeAN from 'react-native-alarm-notification'
import SystemSetting from 'react-native-system-setting'
import RNLockTask from 'react-native-lock-task'
import AlarmStatus from '../NativeAlarm'
import { ALARM_AUDIO as alarmAudio } from '../assets/alarmDismissOptions'

class Home extends React.Component {

    constructor(props) {
        super(props)
        this.state = { intervalId: null, volumeListener: null }
    }

    checkAlarms = () => {
        let currDate = new Date()

        if (this.props.alarms.length > 0 && this.props.currentAlarm === null) {
            let earliestAlarm = this.props.alarms.reduce((prev, curr) => {
                return Date.parse(prev.data.date) > Date.parse(curr.data.date) ? curr : prev
            })
            if (new Date(earliestAlarm.data.date).getTime() < currDate.getTime()) {
                this.triggerAlarm(earliestAlarm)
            }
        }
        this.props.addTime()
    }

    evaluateAlarmType = (alarm) => {
        switch (alarm.data.alarmType) {
            case 'shake':
                this.props.setShakeAlarm(alarm)
                this.props.setShake(alarm.data.dataToUnlock)
                this.switchScreen('shake')
                return
            case 'math':
                this.props.setMathAlarm(alarm)
                this.props.setMath(alarm.data.dataToUnlock)
                this.switchScreen('math')
                return
            case 'pics':
                this.props.setPicAlarm(alarm)
                this.props.setGame(alarm.data.dataToUnlock)
                this.switchScreen('pics')
                return
        }
    }

    switchScreen = (type) => {
        switch (type) {
            case 'shake':
                Actions.shakeViewer({ type: ActionConst.RESET })
                return
            case 'math':
                Actions.mathViewer({ type: ActionConst.RESET })
                return
            case 'pics':
                Actions.picViewer({ type: ActionConst.RESET })
                return
        }
    }

    triggerAlarm = (earliestAlarm) => {
        alarmAudio[earliestAlarm.data.soundIndex].sound.stop()
        AlarmStatus.setAlarming("true");
        this.props.changeIsAlarming(true)
        this.props.setCurrentAlarm(earliestAlarm)
        alarmAudio[earliestAlarm.data.soundIndex].sound.setNumberOfLoops(-1)
        alarmAudio[earliestAlarm.data.soundIndex].sound.play()
        RNLockTask.clearDeviceOwnerApp()
        RNLockTask.startLockTask()
        this.evaluateAlarmType(earliestAlarm)
    }

    componentDidMount() {
        this.props.changeIsHome(true)
        SystemSetting.setVolume(1, {
            playSound: false,
            type: 'music',
            showUI: false
        })

        const listener = SystemSetting.addVolumeListener(data => {
            SystemSetting.setVolume(1, {
                playSound: false,
                type: 'music',
                showUI: false
            })
        })

        const intervalId = setInterval(this.checkAlarms, 1000)

        this.setState({
            volumeListener: listener,
            intervalId
        })

        if (this.props.currentAlarm !== null) {
            this.triggerAlarm(this.props.currentAlarm)
        }
    }

    componentWillUnmount() {
        this.props.changeIsHome(false)
        clearInterval(this.state.intervalId)
        SystemSetting.removeVolumeListener(this.state.volumeListener)
    }

    render() {
        return (
            <ImageBackground source={require('../assets/background.png')} style={{ height: '100%', width: '100%' }}>
                <ScrollView >
                    <View style={{ flexDirection: 'row-reverse' }}>
                        <TouchableOpacity onPress={() => Actions.about()}>
                            <Text style={{ color: 'white', fontSize: 20, marginRight: 10, fontFamily: '' }}>ABOUT</Text>
                        </TouchableOpacity>
                    </View>
                    {(this.props.currentAlarm !== null && this.props.alarms.length > 0 && <View style={{ alignItems: 'center' }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.switchScreen(this.props.currentAlarm.data.alarmType)
                            }}
                            style={{
                                backgroundColor: 'red',
                                width: 300,
                                margin: 10,
                                borderRadius: 5,
                                padding: 3,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                            <Text style={{ color: 'white', fontSize: 30, textAlign: 'center' }}>TURN OFF ALARM</Text>
                        </TouchableOpacity>
                    </View>)}
                    <Card containerStyle={{ borderRadius: 5 }} title={this.props.alarms.length > 0 ? "Alarms" : "No upcoming alarms"}>
                        {this.props.alarms.map((item, index) => (
                            <ListItem containerStyle={{ borderRadius: 5 }} key={index} title={`${new Date(item.data.date).toLocaleDateString()}, ${new Date(item.data.date).toLocaleTimeString()}`}
                                subtitle={item.message}
                                onPressRightIcon={() => {
                                    if (!this.props.currentAlarm) {
                                        this.props.deleteAlarm(index)
                                        ReactNativeAN.deleteAlarm(item.id)
                                    } else {
                                        Alert.alert('Action Failed', 'Please dismiss the current alarm to delete any alarm.')
                                    }
                                }}
                                rightIcon={{ name: 'delete', color: 'blue' }} />
                        ))}
                    </Card>
                    <View style={{ flex: 1, flexDirection: 'row-reverse', alignContent: 'center', alignItems: 'center', margin: 20 }}>
                        <Icon size={50} name="add-circle" color="white" onPress={() => {
                            if (!this.props.currentAlarm) {
                                Actions.createAlarm({ type: ActionConst.RESET })
                            } else {
                                Alert.alert('Action Failed', 'Please dismiss the current alarm to add a new alarm.')
                            }
                        }} />
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    alarms: state.home.alarms,
    reload: state.home.reload,
    activeAlarms: state.home.activeAlarms,
    time: state.home.time,
    shake: state.home.shake,
    isAlarming: state.home.isAlarming,
    currentAlarm: state.home.currentAlarm,
    loading: state.app.isLoading,
    atHome: state.home.atHome,
    finishedAlarmDismiss: state.home.finishedAlarmDismiss,
    sound: state.sound.sound
})

const mapDispatchToProps = {
    reloadComponent,
    changeActive,
    deleteAlarm,
    editAlarm,
    addTime,
    detectActiveAlarms,
    updateShakeCount,
    setCurrentAlarm,
    deleteCurrentAlarm,
    setCurrentAlarmSound,
    setShake,
    resetAlarms,
    isLoading,
    setShakeAlarm,
    setMathAlarm,
    setMath,
    setPicAlarm,
    setGame,
    changeIsHome,
    changeIsAlarming,
    didFinishAlarmDismiss
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)