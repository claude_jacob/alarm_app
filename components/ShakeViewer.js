import React from 'react'
import { View, TouchableOpacity, ImageBackground, BackHandler } from 'react-native'
import { Text } from 'react-native-elements'
import RNShakeEvent from 'react-native-shake-event';
import { updateShake, resetShake, setShake, removeAlarm } from '../actions/shakeActions'
import { connect } from 'react-redux'
import { Actions, ActionConst } from 'react-native-router-flux'
import ReactNativeAN from 'react-native-alarm-notification'
import { deleteAlarm, deleteCurrentAlarm, deleteAlarmById, changeIsAlarming, didFinishAlarmDismiss } from '../actions/homeActions'
import RNLockTask from 'react-native-lock-task'
import SystemSetting from 'react-native-system-setting'
import AlarmStatus from '../NativeAlarm'
import { ALARM_AUDIO as alarmAudio } from '../assets/alarmDismissOptions'

class ShakeViewer extends React.Component {

    state = {
        listener: null
    }

    componentDidMount() {
        RNShakeEvent.addEventListener('shake', () => {
            if (this.props.shake >= 1) {
                this.props.updateShake()
            }
            if (this.props.shake === 1 && this.props.alarm) {
                ReactNativeAN.stopAlarm()
                ReactNativeAN.deleteAlarm(this.props.alarm.id)
                this.props.deleteCurrentAlarm()
                this.props.resetShake()
                this.props.removeAlarm()
                this.props.deleteAlarmById(this.props.alarm.id)
                RNLockTask.stopLockTask()
                AlarmStatus.setAlarming("false")
                this.props.changeIsAlarming(false)
                this.props.didFinishAlarmDismiss(true)
                alarmAudio[this.props.currentAlarm.data.soundIndex].sound.stop()
                Actions.home({ type: ActionConst.RESET })
            }
        })

        SystemSetting.setVolume(1, {
            playSound: false,
            type: 'music',
            showUI: false
        })

        this.props.didFinishAlarmDismiss(false)
        this.props.changeIsAlarming(true)

        const listener = SystemSetting.addVolumeListener(data => {
            if (this.props.currentAlarm !== null) {
                SystemSetting.setVolume(1, {
                    playSound: false,
                    type: 'music',
                    showUI: false
                })
            }
        })

        this.setState({ listener: listener })
    }

    componentWillUnmount() {
        RNShakeEvent.removeEventListener('shake');
        SystemSetting.removeVolumeListener(this.state.listener)
    }

    render() {
        return (
            <ImageBackground source={require('../assets/background.png')} style={{ width: '100%', height: '100%' }}>
                <View style={{ alignItems: 'center', marginTop: 22 }}>
                    <Text h1 style={{ marginTop: 50, color: 'white', fontSize: 100 }}>{this.props.shake}</Text>
                    <Text h3 style={{ marginTop: 20, color: 'white' }}>{this.props.alarm.message}</Text>
                    <View style={{ alignContent: 'center', alignItems: 'center' }}>
                        <Text h4 style={{ marginTop: 70, color: 'white' }}>Shake Device to Turn off Alarm</Text>
                    </View>
                </View>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    alarm: state.shake.alarm,
    shake: state.shake.shakeCount,
    isAlarming: state.home.isAlarming,
    currentAlarm: state.home.currentAlarm,
    finishedAlarmDismiss: state.home.finishedAlarmDismiss
})

const mapDispatchToProps = {
    updateShake,
    resetShake,
    setShake,
    removeAlarm,
    deleteAlarm,
    deleteCurrentAlarm,
    deleteAlarmById,
    changeIsAlarming,
    didFinishAlarmDismiss
}

export default connect(mapStateToProps, mapDispatchToProps)(ShakeViewer)