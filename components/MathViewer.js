import React from 'react'
import { View, TextInput, ImageBackground, TouchableOpacity, KeyboardAvoidingView, ScrollView } from 'react-native'
import { Text } from 'react-native-elements'
import { connect } from 'react-redux'
import { changeSubmitted, handleTextChange, resetMath, removeAlarm } from '../actions/mathActions'
import { deleteCurrentAlarm, deleteAlarmById, changeIsAlarming, didFinishAlarmDismiss } from '../actions/homeActions'
import { Actions, ActionConst } from 'react-native-router-flux'
import ReactNativeAN from 'react-native-alarm-notification'
import RNLockTask from 'react-native-lock-task'
import SystemSetting from 'react-native-system-setting'
import AlarmStatus from '../NativeAlarm'
import { ALARM_AUDIO as alarmAudio } from '../assets/alarmDismissOptions'

class MathViewer extends React.Component {

    state = {
        listener: null
    }

    correctAnswer = () => {
        if (this.props.math.answer === this.props.answer) {
            return true
        }
        return false
    }

    proceed = () => {
        ReactNativeAN.stopAlarm()
        ReactNativeAN.deleteAlarm(this.props.alarm.id)
        this.props.deleteCurrentAlarm()
        this.props.resetMath()
        this.props.removeAlarm()
        this.props.deleteAlarmById(this.props.alarm.id)
        RNLockTask.stopLockTask()
        this.props.changeIsAlarming(false)
        AlarmStatus.setAlarming("false")
        this.props.didFinishAlarmDismiss(true)
        alarmAudio[this.props.currentAlarm.data.soundIndex].sound.stop()
        Actions.home({ type: ActionConst.RESET })
    }

    componentDidMount() {
        this.props.changeIsAlarming(true)
        SystemSetting.setVolume(1, {
            playSound: false,
            type: 'music',
            showUI: false
        })

        this.props.didFinishAlarmDismiss(false)

        const listener = SystemSetting.addVolumeListener(data => {
            if (this.props.currentAlarm !== null) {
                SystemSetting.setVolume(1, {
                    playSound: false,
                    type: 'music',
                    showUI: false
                })
            }
        })

        this.setState({ listener: listener })
        this.props.changeSubmitted(false)
    }

    componentWillUnmount() {
        SystemSetting.removeVolumeListener(this.state.listener)
    }

    render() {
        return (
            <ImageBackground source={require('../assets/background.png')} style={{ width: '100%', height: '100%' }}>
                <ScrollView>
                    <KeyboardAvoidingView behavior="padding" enabled >
                        <View style={{ alignItems: 'center', marginTop: 22 }}>
                            {((this.props.submitted && !this.correctAnswer()) &&
                                <View style={{ backgroundColor: '#ce1023', width: 300, alignItems: 'center', alignContent: 'center' }}>
                                    <Text h4 style={{ color: 'white', padding: 10, textAlign: 'center' }}>Incorrect Anwer!</Text>
                                </View>)}
                            <Text h3 style={{ marginTop: 50, color: 'white' }}>{this.props.math.equation}</Text>
                            <Text style={{ marginTop: 100, color: 'white' }}>Hint: Round the decimal to the nearest hundredth</Text>
                            <TextInput
                                style={{
                                    backgroundColor: '#ffffff',
                                    height: 40,
                                    width: '100%',
                                    margin: 10,
                                    borderRadius: 5,
                                    padding: 3,
                                }}
                                placeholder={'Answer'}
                                returnKeyType='next'
                                onChangeText={this.props.handleTextChange}
                                value={this.props.answer}
                                underlineColorAndroid={'transparent'} />
                            <TouchableOpacity
                                onPress={() => {
                                    if (this.correctAnswer()) {
                                        this.props.resetMath()
                                        this.proceed()
                                    } else {
                                        this.props.changeSubmitted(true)
                                    }
                                }}
                                style={{
                                    backgroundColor: 'white',
                                    height: 40,
                                    width: 300,
                                    margin: 10,
                                    borderRadius: 5,
                                    padding: 3,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    marginTop: 50
                                }}>
                                <Text style={{
                                    color: 'black',
                                    fontSize: 18,
                                    fontWeight: 'bold'
                                }}>SUBMIT</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    submitted: state.math.submitted,
    answer: state.math.answer,
    alarm: state.math.alarm,
    math: state.math.math,
    isAlarming: state.home.isAlarming,
    currentAlarm: state.home.currentAlarm
})

const mapDispatchToProps = {
    changeSubmitted,
    handleTextChange,
    resetMath,
    removeAlarm,
    deleteCurrentAlarm,
    deleteAlarmById,
    changeIsAlarming,
    didFinishAlarmDismiss
}

export default connect(mapStateToProps, mapDispatchToProps)(MathViewer)