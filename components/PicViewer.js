import React from 'react'
import { ImageBackground, View, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, ScrollView } from 'react-native'
import { Text, Card, Badge } from 'react-native-elements'
import { connect } from 'react-redux'
import { reset, updateUserAnswer, changeSubmitted } from '../actions/picsActions'
import { deleteCurrentAlarm, deleteAlarmById, changeIsAlarming, didFinishAlarmDismiss } from '../actions/homeActions'
import ReactNativeAN from 'react-native-alarm-notification'
import RNLockTask from 'react-native-lock-task'
import { Actions, ActionConst } from 'react-native-router-flux'
import SystemSetting from 'react-native-system-setting'
import AlarmStatus from '../NativeAlarm'
import { ALARM_AUDIO as alarmAudio } from '../assets/alarmDismissOptions'

class PicViewer extends React.Component {

    state = {
        listener: null
    }

    correctAnswer = () => {
        if (this.props.game.answer.toLowerCase() === this.props.userAnswer.toLowerCase()) {
            return true
        }
        return false
    }

    proceed = () => {
        ReactNativeAN.stopAlarm()
        ReactNativeAN.deleteAlarm(this.props.alarm.id)
        this.props.deleteCurrentAlarm()
        this.props.deleteAlarmById(this.props.alarm.id)
        RNLockTask.stopLockTask()
        this.props.changeIsAlarming(false)
        this.props.didFinishAlarmDismiss(true)
        AlarmStatus.setAlarming("false")
        alarmAudio[this.props.currentAlarm.data.soundIndex].sound.stop()
        Actions.home({ type: ActionConst.RESET })
    }

    componentDidMount() {
        SystemSetting.setVolume(1, {
            playSound: false,
            type: 'music',
            showUI: false
        })

        this.props.didFinishAlarmDismiss(false)
        this.props.changeIsAlarming(true)

        const listener = SystemSetting.addVolumeListener(data => {
            SystemSetting.setVolume(1, {
                playSound: false,
                type: 'music',
                showUI: false
            })
        })
        this.setState({ listener: listener })
        this.props.changeSubmitted(false)
    }

    componentWillUnmount() {
        SystemSetting.removeVolumeListener(this.state.listener)
    }

    render() {
        return (
            <ImageBackground source={require('../assets/background.png')} style={{ height: '100%', width: '100%' }}>
                <ScrollView>
                    <KeyboardAvoidingView enabled behavior="padding">
                        {
                            this.props.game !== null && <View style={{ alignItems: 'center' }}>
                                <Image source={this.props.game.image} style={{ height: 250, width: 250 }} />
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    {this.props.game.topChoices.map((item, index) => (
                                        <Badge key={'top' + index} value={item} status="success" textStyle={{ fontSize: 25 }} />
                                    ))}
                                </View>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    {this.props.game.bottomChoices.map((item, index) => (
                                        <Badge key={'bot' + index} value={item} status="success" textStyle={{ fontSize: 25 }} />
                                    ))}
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text style={{ fontSize: 15, color: 'white' }}>
                                    Clue: {this.props.game.answer.length} letter word, starting letter "{this.props.game.answer.charAt(0).toUpperCase()}", ends with "{this.props.game.answer.charAt(this.props.game.answer.length -1).toUpperCase()}"
                                    </Text>
                                </View>
                                {((this.props.submitted && !this.correctAnswer()) &&
                                    <View style={{ backgroundColor: '#ce1023', width: 300, alignItems: 'center', alignContent: 'center' }}>
                                        <Text style={{ color: 'white', padding: 10, textAlign: 'center', fontSize: 20 }}>Incorrect Anwer!</Text>
                                    </View>)}
                                <TextInput
                                    style={{
                                        backgroundColor: '#ffffff',
                                        height: 40,
                                        width: '100%',
                                        margin: 10,
                                        borderRadius: 5,
                                        padding: 3,
                                        marginTop: 50
                                    }}
                                    placeholder={'Answer'}
                                    returnKeyType='next'
                                    onChangeText={this.props.updateUserAnswer}
                                    value={this.props.userAnswer}
                                    underlineColorAndroid={'transparent'} />
                                <TouchableOpacity
                                    onPress={() => {
                                        if (this.correctAnswer()) {
                                            this.props.reset()
                                            this.proceed()
                                        } else {
                                            this.props.changeSubmitted(true)
                                        }
                                    }}
                                    style={{
                                        backgroundColor: 'white',
                                        height: 40,
                                        width: 300,
                                        margin: 10,
                                        borderRadius: 5,
                                        padding: 3,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 18,
                                        fontWeight: 'bold'
                                    }}>SUBMIT</Text>
                                </TouchableOpacity>
                            </View>
                        }
                    </KeyboardAvoidingView>
                </ScrollView>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    alarm: state.pics.alarm,
    game: state.pics.game,
    userAnswer: state.pics.userAnswer,
    submitted: state.pics.submitted,
    currentAlarm: state.home.currentAlarm
})

const mapDispatchToProps = {
    reset,
    updateUserAnswer,
    deleteCurrentAlarm,
    deleteAlarmById,
    changeIsAlarming,
    changeSubmitted,
    didFinishAlarmDismiss
}

export default connect(mapStateToProps, mapDispatchToProps)(PicViewer)