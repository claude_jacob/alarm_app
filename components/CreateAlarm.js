import React from 'react'
import { Modal, View, Picker, ImageBackground, ScrollView, TouchableOpacity, Alert, TextInput } from 'react-native'
import { Card, Text, CheckBox, ListItem } from 'react-native-elements'
import { connect } from 'react-redux'
import { Actions, ActionConst } from 'react-native-router-flux'
import { handleAlarmNameChange, editAlarm, changeModalVisible, playRingtone, upadteRingtone, updateDate, showDateTimePicker, changeAlarmType, updateShakeCount, updateDifficulty } from '../actions/createAlarmActions'
import { createAlarm, saveEditedAlarm } from '../actions/homeActions'
import DateTimePicker from 'react-native-modal-datetime-picker'
import ReactNativeAN from 'react-native-alarm-notification';
import { ALARM_AUDIO as alarmAudio, mathProblemCategories, picturesGame } from '../assets/alarmDismissOptions'

class CreateAlarm extends React.Component {

    randomNum = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    getUniqueId = () => {
        let characters = '1234567890'
        let uniqueId = ''
        for (let x = 0; x < 8; x++) {
            uniqueId += characters.charAt(this.randomNum(0, 9))
        }
        return uniqueId
    }

    getRandomPicture = () => {
        return picturesGame[this.randomNum(0, picturesGame.length - 1)]
    }

    getRandomMath = (difficulty) => {
        switch (difficulty) {
            case 'easy':
                return mathProblemCategories.easy[this.randomNum(0, mathProblemCategories.easy.length - 1)]
            case 'medium':
                return mathProblemCategories.medium[this.randomNum(0, mathProblemCategories.medium.length - 1)]
            case 'hard':
                return mathProblemCategories.hard[this.randomNum(0, mathProblemCategories.hard.length - 1)]
        }
    }

    getShakeCountArray = max => {
        let arr = []
        for (let x = 30; x <= max; x++) {
            arr.push(`${x}`)
        }
        return arr
    }

    getMilitaryTime = () => {
        let hour = parseInt(this.props.hour)
        if (this.props.daytime === 'pm' && hour < 12) hour += 12
        if (this.props.daytime === 'am' && hour === 12) hour -= 12
        return hour + ':' + this.props.minute
    }

    resetCreateAlarm = () => {
        this.props.updateDate(new Date())
        this.props.ringtone.sound.stop()
        this.props.playRingtone(false)
    }

    getRepeat = () => {
        let repeat = []
        for (let day of this.props.repeat) {
            if (day.checked) {
                repeat.push(day)
            }
        }
        return repeat
    }

    componentDidMount() {
        this.props.ringtone.sound.setCategory('Playback')
    }

    addAlarm = () => {
        const alarmNotifData = {
            id: this.getUniqueId(),
            title: "Alarm",
            message: this.props.alarmName,
            channel: "my_channel_id",
            ticker: "My Notification Ticker",
            auto_cancel: false,
            vibrate: true,
            vibration: 400,
            small_icon: "ic_launcher",
            large_icon: "ic_launcher",
            play_sound: false,
            sound_name: this.props.ringtone.fileName,
            color: "red",
            schedule_once: true,
            tag: 'some_tag',
            fire_date: ReactNativeAN.parseDate(this.props.date),
            data: { date: this.props.date, alarmType: this.props.alarmType, dataToUnlock: this.getDataToDismiss(this.props.alarmType), soundIndex: this.props.ringtone.index },
        };
        this.props.createAlarm(alarmNotifData)
        ReactNativeAN.scheduleAlarm(alarmNotifData)
    }

    getDataToDismiss = type => {
        switch (type) {
            case 'shake':
                return this.props.shakeCount
            case 'math':
                return this.getRandomMath(this.props.difficulty)
            case 'pics':
                return this.getRandomPicture()
        }
    }

    render() {
        return (
            <ImageBackground source={require('../assets/background.png')} style={{ width: '100%', height: '100%' }}>
                <ScrollView>
                    <Modal
                        animationType="slide"
                        transparent={false}
                        onRequestClose={() => console.log('closing')}
                        visible={this.props.modalVisible}>
                        {alarmAudio.map((item, index) => (
                            <ListItem key={'modalitem' + index} title={item.displayName} onPress={() => {
                                this.props.upadteRingtone(item)
                                this.props.changeModalVisible()
                            }} />
                        ))
                        }
                        <View style={{ alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={this.props.changeModalVisible}
                                style={{
                                    backgroundColor: 'white',
                                    height: 40,
                                    width: 150,
                                    margin: 10,
                                    borderRadius: 5,
                                    padding: 3,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                <Text style={{
                                    color: 'black',
                                    fontSize: 18,
                                    fontWeight: 'bold'
                                }}>CANCEL</Text>
                            </TouchableOpacity>
                        </View>
                    </Modal>
                    <DateTimePicker
                        mode="datetime"
                        isVisible={this.props.isDateTimePickerVisible}
                        minimumDate={new Date()}
                        onConfirm={date => {
                            this.props.updateDate(date)
                            this.props.showDateTimePicker(false)
                        }}
                        onCancel={() => this.props.showDateTimePicker(false)}
                    />
                    <View style={{ alignItems: 'center', borderWidth: 2, borderRadius: 15, marginTop: 50, borderColor: 'white' }}>
                        <Text style={{ color: 'white' }} h3>{this.props.date.toLocaleDateString()}, {this.props.date.toLocaleTimeString()}</Text>
                    </View>
                    <View style={{ alignItems: 'center', marginTop: 20 }}>
                        <TextInput
                            style={{
                                backgroundColor: '#ffffff',
                                height: 40,
                                width: '100%',
                                margin: 10,
                                borderRadius: 5,
                                padding: 3,
                            }}
                            placeholder={'Alarm Name'}
                            returnKeyType='next'
                            onChangeText={this.props.handleAlarmNameChange}
                            value={this.props.alarmName}
                            underlineColorAndroid={'transparent'} />
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <TouchableOpacity
                            onPress={() => this.props.showDateTimePicker(true)}
                            style={{
                                backgroundColor: 'white',
                                margin: 10,
                                height: 40,
                                width: 300,
                                borderRadius: 5,
                                padding: 3,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                            <Text style={{
                                color: 'black',
                                fontSize: 18,
                                fontWeight: 'bold'
                            }}>Select Date and Time</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Card containerStyle={{ borderRadius: 5 }} title="Choose Alarm Type">
                            <CheckBox
                                style={{ width: 300 }}
                                title="Shake Device"
                                checked={this.props.checkShake}
                                onPress={() => this.props.changeAlarmType('shake')}
                            />
                            <CheckBox
                                style={{ width: 300 }}
                                title="Math Problems"
                                checked={this.props.checkMath}
                                onPress={() => this.props.changeAlarmType('math')}
                            />
                            <CheckBox
                                style={{ width: 300 }}
                                title="4 pics one word"
                                checked={this.props.checkPics}
                                onPress={() => this.props.changeAlarmType('pics')}
                            />
                        </Card>
                    </View>
                    {(this.props.checkShake && <View>
                        <Card containerStyle={{ borderRadius: 5 }} title="Shake Count">
                            <Picker style={{ width: 300 }} onValueChange={item => {
                                this.props.updateShakeCount(item)
                            }}
                                selectedValue={this.props.shakeCount}
                            >
                                {this.getShakeCountArray(1000).map((item, index) => (
                                    <Picker.Item key={'shake' + index} label={item} value={item} />
                                ))}
                            </Picker>
                        </Card>
                    </View>)}
                    {(this.props.checkMath && <View>
                        <Card containerStyle={{ borderRadius: 5 }} title="Difficulty">
                            <Picker style={{ width: 300 }} onValueChange={item => {
                                this.props.updateDifficulty(item)
                            }}
                                selectedValue={this.props.difficulty}
                            >
                                <Picker.Item label={'Easy'} value={'easy'} />
                                <Picker.Item label={'Medium'} value={'medium'} />
                                <Picker.Item label={'Hard'} value={'hard'} />
                            </Picker>
                        </Card>
                    </View>)}
                    <View>
                        <Card containerStyle={{ borderRadius: 5 }}>
                            <ListItem title="Ringtone" onPress={() => {
                                this.props.ringtone.sound.stop()
                                this.props.playRingtone(false)
                                this.props.changeModalVisible()
                            }} subtitle={this.props.ringtone.displayName}
                                onPressRightIcon={() => {
                                    if (this.props.ringtonePlaying) {
                                        this.props.ringtone.sound.stop()
                                        this.props.playRingtone(false)
                                    } else {
                                        this.props.ringtone.sound.play()
                                        this.props.playRingtone(true)
                                    }
                                }}
                                rightIcon={{ name: this.props.ringtonePlaying ? 'pause-circle-outline' : 'play-circle-outline', size: 40 }} />
                        </Card>
                    </View>
                    <View style={{ alignItems: 'center', marginTop: 50 }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.resetCreateAlarm()
                                    Actions.home({ type: ActionConst.RESET })
                                }}
                                style={{
                                    backgroundColor: 'white',
                                    height: 40,
                                    width: 150,
                                    margin: 10,
                                    borderRadius: 5,
                                    padding: 3,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                <Text style={{
                                    color: 'black',
                                    fontSize: 18,
                                    fontWeight: 'bold'
                                }}>CANCEL</Text>
                            </TouchableOpacity>
                            <View style={{ width: 20 }} />
                            <TouchableOpacity
                                onPress={async () => {
                                    await this.addAlarm()
                                    await this.resetCreateAlarm()
                                    Actions.home({ type: ActionConst.RESET })
                                }}
                                style={{
                                    backgroundColor: 'white',
                                    height: 40,
                                    width: 150,
                                    margin: 10,
                                    borderRadius: 5,
                                    padding: 3,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                <Text style={{
                                    color: 'black',
                                    fontSize: 18,
                                    fontWeight: 'bold'
                                }}>OK</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground >
        )
    }
}

const mapStateToProps = state => ({
    date: state.createAlarm.date,
    active: state.createAlarm.active,
    ringtone: state.createAlarm.ringtone,
    ringtonePlaying: state.createAlarm.ringtonePlaying,
    modalVisible: state.createAlarm.modalVisible,
    alarmName: state.createAlarm.alarmName,
    isDateTimePickerVisible: state.createAlarm.isDateTimePickerVisible,
    alarmType: state.createAlarm.alarmType,
    checkShake: state.createAlarm.checkShake,
    checkMath: state.createAlarm.checkMath,
    checkPics: state.createAlarm.checkPics,
    shakeCount: state.createAlarm.shakeCount,
    difficulty: state.createAlarm.difficulty
})

const mapDispatchToProps = {
    createAlarm,
    upadteRingtone,
    playRingtone: playRingtone,
    changeModalVisible,
    editAlarm,
    saveEditedAlarm,
    handleAlarmNameChange,
    updateDate,
    showDateTimePicker,
    changeAlarmType,
    updateShakeCount,
    updateDifficulty
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateAlarm)