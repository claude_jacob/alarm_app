import { default as Sound } from 'react-native-sound'

export const ALARM_AUDIO = [
    { displayName: 'bomb-siren', sound: new Sound('bombsiren.mp3', Sound.MAIN_BUNDLE), fileName: 'bombsiren.mp3', index: 0 },
    { displayName: 'analog-watch', sound: new Sound('analogwatch.mp3', Sound.MAIN_BUNDLE), fileName: 'analogwatch.mp3', index: 1 },
    { displayName: 'tornado-siren', sound: new Sound('tornadosiren.mp3', Sound.MAIN_BUNDLE), fileName: 'tornadosiren.mp3', index: 2 },
    { displayName: 'submarine-alarm', sound: new Sound('submarinealarm.mp3', Sound.MAIN_BUNDLE), fileName: 'submarinealarm.mp3', index: 3 },
]

export const mathProblemCategories = {
    easy: [
        { equation: '5*5+4-3+2-20', answer: '8' },
        { equation: '4+4-3+5*2%2', answer: '10' },
        { equation: '3*3+1-5+4-10', answer: '-1' },
        { equation: '2*2%2+6+2-10', answer: '0' },
        { equation: '1+9*3%2+5-10', answer: '9.5' },
        { equation: '8*10+20%2-40', answer: '50' },
    ],
    medium: [
        { equation: '11+56-9×8', answer: '-5' },
        { equation: '12+24-67+34', answer: '3' },
        { equation: '17+43+11%2', answer: '65.5' },
        { equation: '65+34*2', answer: '133' },
        { equation: '64÷8*10-40', answer: '40' },
        { equation: '87-56-11*2%4', answer: '25.5' },
    ],
    hard: [
        { equation: '2+111+12+24*2', answer: '173' },
        { equation: '5+6*10+9+72', answer: '146' },
        { equation: '2-9*5+165+34', answer: '156' },
        { equation: '8+17+43-20*2', answer: '28' },
        { equation: '3-64%8*5%2', answer: '-17' },
        { equation: '4+67-34-13%2', answer: '30.5' },
    ]
}

export const picturesGame = [
    {
        answer: 'swimsuit', image: require('./game/swimsuit.png'), topChoices: ['S', 'W', 'T', 'M', 'W', 'Q'],
        bottomChoices: ['U', 'I', 'S', 'U', 'G', 'Y']
    },
    {
        answer: 'teardrop', image: require('./game/teardrop.png'), topChoices: ['Q', 'A', 'P', 'O', 'E', 'Q'],
        bottomChoices: ['R', 'D', 'T', 'R', 'V', 'X']
    },
    {
        answer: 'timeline', image: require('./game/timeline.png'), topChoices: ['I', 'N', 'Y', 'O', 'E', 'A'],
        bottomChoices: ['I', 'M', 'E', 'L', 'U', 'T']
    },
    {
        answer: 'transfer', image: require('./game/transfer.png'), topChoices: ['X', 'F', 'Q', 'R', 'O', 'E'],
        bottomChoices: ['A', 'N', 'U', 'T', 'S', 'R']
    },
    {
        answer: 'transmit', image: require('./game/transmit.png'), topChoices: ['R', 'L', 'M', 'A', 'Q', 'S'],
        bottomChoices: ['I', 'Y', 'A', 'T', 'T', 'N']
    },
    {
        answer: 'tropical', image: require('./game/tropical.png'), topChoices: ['C', 'C', 'Y', 'R', 'L', 'I'],
        bottomChoices: ['P', 'U', 'O', 'A', 'T', 'D']
    },
    {
        answer: 'trousers', image: require('./game/trousers.png'), topChoices: ['U', 'A', 'O', 'R', 'Q', 'V'],
        bottomChoices: ['I', 'E', 'S', 'R', 'S', 'T']
    },
    {
        answer: 'umbrella', image: require('./game/umbrella.png'), topChoices: ['U', 'D', 'A', 'L', 'V', 'R'],
        bottomChoices: ['L', 'B', 'Q', 'M', 'E', 'Y']
    },
    {
        answer: 'unicycle', image: require('./game/unicycle.png'), topChoices: ['L', 'U', 'I', 'A', 'Y', 'I'],
        bottomChoices: ['M', 'C', 'O', 'N', 'E', 'C']
    },
    {
        answer: 'vacation', image: require('./game/vacation.png'), topChoices: ['I', 'S', 'G', 'A', 'O', 'V'],
        bottomChoices: ['A', 'N', 'T', 'R', 'C', 'K']
    },
    {
        answer: 'wellness', image: require('./game/wellness.png'), topChoices: ['L', 'I', 'W', 'E', 'H', 'E'],
        bottomChoices: ['S', 'E', 'N', 'Q', 'V', 'L']
    },
    {
        answer: 'withdraw', image: require('./game/withdraw.png'), topChoices: ['I', 'U', 'C', 'A', 'H', 'N'],
        bottomChoices: ['R', 'W', 'T', 'Y', 'D', 'W']
    },
    {
        answer: 'workshop', image: require('./game/workshop.png'), topChoices: ['R', 'E', 'S', 'N', 'P', 'O'],
        bottomChoices: ['O', 'W', 'P', 'K', 'V', 'H']
    }
]