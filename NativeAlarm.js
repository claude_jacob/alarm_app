import { NativeModules } from 'react-native'
const AlarmModule = NativeModules.AlarmModule
const RNAlarm = {}

RNAlarm.setAlarming = value => {
    AlarmModule.setAlarming(value)
}

export default RNAlarm