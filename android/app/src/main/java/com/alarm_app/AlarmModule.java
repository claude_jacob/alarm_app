package com.alarm_app;

import android.content.Intent;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import java.lang.Boolean;

public class AlarmModule extends ReactContextBaseJavaModule {

    static boolean alarming;

    public AlarmModule(ReactApplicationContext reactContext){
        super(reactContext);
    }

    @Override
    public String getName(){
        return "AlarmModule";
    }

    @ReactMethod
    public void setAlarming(String value){
        Boolean booleanValue = Boolean.valueOf(value);
        this.alarming = booleanValue;
    }
}