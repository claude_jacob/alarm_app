package com.alarm_app;

import com.facebook.react.ReactActivity;
import android.os.Bundle;
import org.json.JSONObject;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import android.content.Intent;
import com.emekalites.react.alarm.notification.BundleJSONConverter;
import java.lang.Exception;
import android.view.WindowManager;
import android.view.Window;
import com.emekalites.react.alarm.notification.ANAlarmReceiver;

public class MainActivity extends ReactActivity {
    
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "alarm_app";
    }
     
    @Override
    public void onNewIntent(Intent intent) {
        // Do whatever you need to do 
        // e.g. 
        super.onNewIntent(intent);
        try{
            Bundle bundle = intent.getExtras();
            JSONObject data = BundleJSONConverter.convertToJSON(bundle);
            getReactInstanceManager().getCurrentReactContext().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("OnNotificationOpened", data.toString());
        }catch(Exception e){
            System.out.println("Error");
        }
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        if(getIntent().hasExtra(ANAlarmReceiver.WAKE) && getIntent().getExtras().getBoolean(ANAlarmReceiver.WAKE)){
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        if(AlarmModule.alarming) {
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }
    }

    @Override
    public void onStop(){
        super.onStop();
        if(AlarmModule.alarming) {
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if(AlarmModule.alarming) {
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }
    }
}
